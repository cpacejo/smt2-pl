:- module(smt2, [
    op(760, yfx, #<==>),
    op(750, xfy, #==>),
    op(750, yfx, #<==),
    op(740, yfx, #\/),
    op(730, yfx, #\),
    op(720, yfx, #/\),
    op(710, fy, #\),
    op(700, xfx, #>),
    op(700, xfx, #<),
    op(700, xfx, #>=),
    op(700, xfx, #=<),
    op(700, xfx, #=),
    op(700, xfx, #\=),
    op(700, xfx, in),
    op(700, xfx, ins),
    op(450, xfx, ..),
    (#>)/2, (#<)/2, (#>=)/2, (#=<)/2,
    (#=)/2, (#\=)/2, (#\)/1,
    (#<==>)/2, (#==>)/2, (#<==)/2,
    (#\/)/2, (#\)/2, (#/\)/2,
    (in)/2, (ins)/2,
    all_distinct/1, chain/2,
    indomain/1, check_sat/0, label/1
]).

% TODO:
% * attribute_goals/3 / get-assertions
% * user control over configuration
% * allow user to disable constraint sat checks

:- use_module(library(apply)).
:- use_module(library(debug)).
:- use_module(library(gensym)).
:- use_module(library(process)).


%
% configuration
%

solver(z3).
%solver(cvc4).

constraint_timeout_ms(10).
label_timeout_ms(30000).


%
% I/O
%

smt2_out(ToSMT2, A) :-
    atom(A), !,
    format(ToSMT2, "~a", [A]).
smt2_out(ToSMT2, I) :-
    integer(I), !,
    format(ToSMT2, "~d", [I]).
smt2_out(ToSMT2, []) :- !,
    format(ToSMT2, "()", []).
smt2_out(ToSMT2, [H|T]) :- !,
    put_char(ToSMT2, 0'(),
    smt2_out(ToSMT2, H),
    maplist(smt2_out_aux(ToSMT2), T),
    put_char(ToSMT2, 0')).
smt2_out_aux(ToSMT2, E) :-
    put_char(ToSMT2, 0' ),
    smt2_out(ToSMT2, E).

smt2_out(Term) :-
    nb_getval(smt2_io, io(ToSMT2, _)),
    %smt2_out(user_output, Term), nl(user_output),
    smt2_out(ToSMT2, Term), nl(ToSMT2).


smt2_flush :-
    nb_getval(smt2_io, io(ToSMT2, _)),
    flush_output(ToSMT2).


smt2_in_sym(FromSMT2, Chars) :-
    peek_char(FromSMT2, C),
    ((char_type(C, space); C = '('; C = ')') -> Chars = [];
        get_char(FromSMT2, _), Chars = [C | Chars0], smt2_in_sym(FromSMT2, Chars0)).

smt2_in_num(FromSMT2, Chars) :-
    peek_char(FromSMT2, C),
    (char_type(C, digit) ->
        get_char(FromSMT2, _), Chars = [C | Chars0], smt2_in_num(FromSMT2, Chars0);
        Chars = []).

smt2_in_list(FromSMT2, Elts) :-
    smt2_in(FromSMT2, Elt) -> Elts = [Elt | Elts0], smt2_in_list(FromSMT2, Elts0);
    get_char(FromSMT2, C), assertion(C = ')'), Elts = [].

smt2_in(FromSMT2, Term) :-
    peek_char(FromSMT2, C),
    (char_type(C, space) -> get_char(FromSMT2, _), smt2_in(FromSMT2, Term);
        C = '(' -> get_char(FromSMT2, _), smt2_in_list(FromSMT2, Term);
        char_type(C, digit) -> smt2_in_num(FromSMT2, Chars), number_chars(Term, Chars);
        C \= ')' -> smt2_in_sym(FromSMT2, Chars), atom_chars(Term, Chars)).

smt2_in(Term) :-
    nb_getval(smt2_io, io(_, FromSMT2)),
    smt2_in(FromSMT2, Term).
    %smt2_out(user_output, Term), nl(user_output).


smt2_op(Out, In) :-
    smt2_out(Out),
    smt2_flush,
    smt2_in(In).

smt2_op(Out) :-
    smt2_op(Out, In),
    assertion(In = success).


set_timeout(MS) :-
    solver(Solver), set_timeout(Solver, MS).

set_timeout(z3, MS) :-
    smt2_op(['set-option', ':timeout', MS]).

set_timeout(cvc4, _MS).
    % This appears only to work the first time.
    %smt2_op(['set-option', ':tlimit-per', MS]).


%
% translation
%

expr_as_smt2(VV, T, Vs) :- var(VV), !,
    expr_as_smt2(?(VV), T, Vs).

expr_as_smt2(?(VV), V, []) :- get_attr(VV, smt2, Attr), !,
    Attr = int(V).

expr_as_smt2(?(VV), V, [V]) :- var(VV), !,
    gensym(i, V),
    put_attr(VV, smt2, int(V)).

expr_as_smt2(I, [-, In], []) :- integer(I), I < 0, !, In is -I.

expr_as_smt2(I, I, []) :- integer(I), !.

expr_as_smt2(-A, [-, At], Vs) :-
    expr_as_smt2(A, At, Vs).

expr_as_smt2(A + B, [+, At, Bt], Vs) :-
    expr_as_smt2(A, At, Vs0),
    expr_as_smt2(B, Bt, Vs1),
    append(Vs0, Vs1, Vs).

expr_as_smt2(A - B, [-, At, Bt], Vs) :-
    expr_as_smt2(A, At, Vs0),
    expr_as_smt2(B, Bt, Vs1),
    append(Vs0, Vs1, Vs).

expr_as_smt2(A * B, [*, At, Bt], Vs) :-
    expr_as_smt2(A, At, Vs0),
    expr_as_smt2(B, Bt, Vs1),
    append(Vs0, Vs1, Vs).

expr_as_smt2(A div B, [div, At, Bt], Vs) :-
    expr_as_smt2(A, At, Vs0),
    expr_as_smt2(B, Bt, Vs1),
    append(Vs0, Vs1, Vs).

expr_as_smt2(A mod B, [mod, At, Bt], Vs) :-
    expr_as_smt2(A, At, Vs0),
    expr_as_smt2(B, Bt, Vs1),
    append(Vs0, Vs1, Vs).

expr_as_smt2(abs(A), [abs, At], Vs) :-
    expr_as_smt2(A, At, Vs).


pred_as_smt2(VV, T, IVs, BVs) :- var(VV), !,
    pred_as_smt2(?(VV), T, IVs, BVs).

pred_as_smt2(?(VV), V, [], []) :- get_attr(VV, smt2, Attr), !,
    Attr = bool(V).

pred_as_smt2(?(VV), V, [], [V]) :- var(VV), !,
    gensym(b, V),
    put_attr(VV, smt2, bool(V)).

pred_as_smt2(true, true, [], []).

pred_as_smt2(true, false, [], []).

pred_as_smt2(A in I, [=, At, I], IVs, []) :- integer(I), !,
    expr_as_smt2(A, At, IVs).

pred_as_smt2(A in inf..sup, true, IVs, []) :- !,
    expr_as_smt2(A, _, IVs).

pred_as_smt2(A in L..sup, [<=, L, At], IVs, []) :- integer(L), !,
    expr_as_smt2(A, At, IVs).

pred_as_smt2(A in inf..U, [<=, At, U], IVs, []) :- integer(U), !,
    expr_as_smt2(A, At, IVs).

pred_as_smt2(A in L..U, [<=, L, At, U], IVs, []) :- integer(L), integer(U), !,
    expr_as_smt2(A, At, IVs).

pred_as_smt2(A in B \/ C, [or, ABt, ACt], IVs, BVs) :-
    pred_as_smt2(A in B, ABt, IVs0, BVs0),
    pred_as_smt2(A in C, ACt, IVs1, BVs1),
    append(IVs0, IVs1, IVs), append(BVs0, BVs1, BVs).

pred_as_smt2(all_distinct(As), [distinct | Ats], IVs, []) :-
    maplist(expr_as_smt2, As, Ats, IVss),
    append(IVss, IVs).

% I don't know that these actually help anything,
% and they don't work in older versions of Z3.
%pred_as_smt2(A mod K #= 0, [['_', divisible, K], At], IVs, []) :- integer(K), K > 0, !,
%    expr_as_smt2(A, At, IVs).

%pred_as_smt2(0 #= A mod K, [['_', divisible, K], At], IVs, []) :- integer(K), K > 0, !,
%    expr_as_smt2(A, At, IVs).

pred_as_smt2(chain([], _), true, [], []) :- !.

pred_as_smt2(chain([A], _), true, IVs, []) :- !,
    expr_as_smt2(A, _, IVs).

pred_as_smt2(A #= B, T, IVs, BVs) :-
    pred_as_smt2(chain([A, B], #=), T, IVs, BVs).

pred_as_smt2(chain(As, #=), [= | Ats], IVs, []) :- !,
    maplist(expr_as_smt2, As, Ats, IVss),
    append(IVss, IVs).

pred_as_smt2(A #\= B, [not, [=, At, Bt]], IVs, []) :-
    expr_as_smt2(A, At, IVs0),
    expr_as_smt2(B, Bt, IVs1),
    append(IVs0, IVs1, IVs).

pred_as_smt2(A #>= B, T, IVs, BVs) :-
    pred_as_smt2(chain([A, B], #>=), T, IVs, BVs).

pred_as_smt2(chain(As, #>=), [>= | Ats], IVs, []) :- !,
    maplist(expr_as_smt2, As, Ats, IVss),
    append(IVss, IVs).

pred_as_smt2(A #=< B, T, IVs, BVs) :-
    pred_as_smt2(chain([A, B], #=<), T, IVs, BVs).

pred_as_smt2(chain(As, #=<), [<= | Ats], IVs, []) :- !,
    maplist(expr_as_smt2, As, Ats, IVss),
    append(IVss, IVs).

pred_as_smt2(A #> B, T, IVs, BVs) :-
    pred_as_smt2(chain([A, B], #>), T, IVs, BVs).

pred_as_smt2(chain(As, #>), [> | Ats], IVs, []) :- !,
    maplist(expr_as_smt2, As, Ats, IVss),
    append(IVss, IVs).

pred_as_smt2(A #< B, T, IVs, BVs) :-
    pred_as_smt2(chain([A, B], #<), T, IVs, BVs).

pred_as_smt2(chain(As, #<), [< | Ats], IVs, []) :- !,
    maplist(expr_as_smt2, As, Ats, IVss),
    append(IVss, IVs).

pred_as_smt2(#\ A, [not, At], IVs, BVs) :-
    pred_as_smt2(A, At, IVs, BVs).

pred_as_smt2(A #\/ B, [or, At, Bt], IVs, BVs) :-
    pred_as_smt2(A, At, IVs0, BVs0),
    pred_as_smt2(B, Bt, IVs1, BVs1),
    append(IVs0, IVs1, IVs),
    append(BVs0, BVs1, BVs).

pred_as_smt2(A #/\ B, [and, At, Bt], IVs, BVs) :-
    pred_as_smt2(A, At, IVs0, BVs0),
    pred_as_smt2(B, Bt, IVs1, BVs1),
    append(IVs0, IVs1, IVs),
    append(BVs0, BVs1, BVs).

pred_as_smt2(A #\ B, [xor, At, Bt], IVs, BVs) :-
    pred_as_smt2(A, At, IVs0, BVs0),
    pred_as_smt2(B, Bt, IVs1, BVs1),
    append(IVs0, IVs1, IVs),
    append(BVs0, BVs1, BVs).

pred_as_smt2(A #<==> B, [=, At, Bt], IVs, BVs) :-
    pred_as_smt2(A, At, IVs0, BVs0),
    pred_as_smt2(B, Bt, IVs1, BVs1),
    append(IVs0, IVs1, IVs),
    append(BVs0, BVs1, BVs).

pred_as_smt2(A #==> B, [=>, At, Bt], IVs, BVs) :-
    pred_as_smt2(A, At, IVs0, BVs0),
    pred_as_smt2(B, Bt, IVs1, BVs1),
    append(IVs0, IVs1, IVs),
    append(BVs0, BVs1, BVs).

pred_as_smt2(A #<== B, [=>, Bt, At], IVs, BVs) :-
    pred_as_smt2(A, At, IVs0, BVs0),
    pred_as_smt2(B, Bt, IVs1, BVs1),
    append(IVs0, IVs1, IVs),
    append(BVs0, BVs1, BVs).


smt2_as_assn([Var, [-, Val]], Var - Valn) :- integer(Val), !, Valn is -Val.
smt2_as_assn([Var, Val], Var - Val) :- integer(Val), !.
smt2_as_assn([Var, true], Var - true) :- !.
smt2_as_assn([Var, false], Var - false) :- !.

smt2_as_assns(SMT2Assns, Assns) :-
    maplist(smt2_as_assn, SMT2Assns, Assns).

assn_as_smt2_expr(Var - Val, [=, Var, [-, Valn]]) :- integer(Val), Val < 0, !, Valn is -Val.
assn_as_smt2_expr(Var - Val, [=, Var, Val]) :- integer(Val), !.
assn_as_smt2_expr(Var - true, Var) :- !.
assn_as_smt2_expr(Var - false, [not, Var]) :- !.


%
% engine
%

adjust_level :-
    % FIXME: can we "commit" when choice points collapse?
    % both to elimintate no-longer-needed pushes
    % and to prevent extra push after cut since choice point # "changes"
    nb_getval(smt2_io_level, IoLevel),
    b_getval(smt2_pl_level, level(PlLevel, LastChoice)),
    (IoLevel > PlLevel -> ToPop is IoLevel - PlLevel, smt2_op([pop, ToPop]);
        assertion(IoLevel = PlLevel)),
    prolog_current_choice(CurChoice),
    (CurChoice = LastChoice -> nb_setval(smt2_io_level, PlLevel);
        smt2_op([push, 1]),
        NewLevel is PlLevel + 1,
        nb_setval(smt2_io_level, NewLevel),
        b_setval(smt2_pl_level, level(NewLevel, CurChoice))).


constrain(int, V) :-
    smt2_op(['declare-fun', V, [], 'Int']).

constrain(bool, V) :-
    smt2_op(['declare-fun', V, [], 'Bool']).

smt2_assert(SMT2Pred, IVs, BVs) :-
    adjust_level,
    maplist(constrain(int), IVs),
    maplist(constrain(bool), BVs),
    smt2_op([assert, SMT2Pred]),
    smt2_op(['check-sat'], Resp),
    assertion(member(Resp, [sat, unsat, unknown])),
    Resp \= unsat.

smt2_assert(Pred) :-
    pred_as_smt2(Pred, SMT2Pred, IVs, BVs),
    smt2_assert(SMT2Pred, IVs, BVs).

smt2_assert_dom(Dom, A) :- A in Dom.

smt2_assert_label(Var, Val) :-
    adjust_level,
    smt2_op([assert, [=, Var, Val]]),
    % Unifying with a ground value is essentially labeling.
    % Since we're leaving control of the solver, we must ensure
    % the validity of this labeling; hence a full `check_sat`.
    check_sat.

attr_unify_hook(int(V), VV) :-
    get_attr(VV, smt2, Attr) ->
        Attr = int(V1),
        smt2_assert([=, V, V1], [], []);
    var(VV) ->
        put_attr(VV, smt2, int(V));
    integer(VV), VV < 0 ->
        VVn is -VV,
        smt2_assert_label(V, [-, VVn]);
    integer(VV) ->
        smt2_assert_label(V, VV).

attr_unify_hook(bool(V), VV) :-
    get_attr(VV, smt2, Attr) ->
        Attr = bool(V1),
        smt2_assert([=, V, V1], [], []);
    var(VV) ->
        put_attr(VV, smt2, bool(V));
    (VV = true; VV = false) ->
        smt2_assert_label(V, VV).


%
% constraints
%

A in B :- smt2_assert(A in B).
As ins B :- maplist(smt2_assert_dom(B), As).
A #= B :- smt2_assert(A #= B).
A #\= B :- smt2_assert(A #\= B).
A #>= B :- smt2_assert(A #>= B).
A #=< B :- smt2_assert(A #=< B).
A #> B :- smt2_assert(A #> B).
A #< B :- smt2_assert(A #< B).
#\ A :- smt2_assert(#\ A).
A #\/ B :- smt2_assert(A #\/ B).
A #/\ B :- smt2_assert(A #/\ B).
A #\ B :- smt2_assert(A #\ B).
A #<==> B :- smt2_assert(A #<==> B).
A #==> B :- smt2_assert(A #==> B).
A #<== B :- smt2_assert(A #<== B).
all_distinct(Zs) :- smt2_assert(all_distinct(Zs)).
chain(Zs, Rel) :- smt2_assert(chain(Zs, Rel)).


%
% labeling
%

smt2_attr(VV, V) :- get_attr(VV, smt2, V).

attr_var(int(V), V).
attr_var(bool(V), V).

apply_labels([VV | Vars], [V - A | Assns]) :-
    get_attr(VV, smt2, Attr), !,
    attr_var(Attr, V), del_attr(VV, smt2),
    VV = A, apply_labels(Vars, Assns).

apply_labels([_ | Vars], Assns) :-
    apply_labels(Vars, Assns).

apply_labels([], []).

check_sat :-
    label_timeout_ms(LabelTimeoutMS), set_timeout(LabelTimeoutMS),
    smt2_op(['check-sat'], SatResp),
    constraint_timeout_ms(ConstraintTimeoutMS), set_timeout(ConstraintTimeoutMS),
    assertion(member(SatResp, [sat, unsat, unknown])),
    (SatResp = unknown -> throw(smt2_labeling_error); SatResp = sat).

conjoin_exprs([], true) :- !.
conjoin_exprs([E], E) :- !.
conjoin_exprs(Es, [and | Es]).

label(Vars) :-
    convlist(smt2_attr, Vars, Attrs),
    maplist(attr_var, Attrs, SMT2Vars),
    check_sat,
    (SMT2Vars = [] -> true;
        smt2_op(['get-value', SMT2Vars], AssnResp),
        smt2_as_assns(AssnResp, Assns),
        maplist(assn_as_smt2_expr, Assns, AssnExprs),
        conjoin_exprs(AssnExprs, AssnExpr),
        (
            adjust_level, smt2_op([assert, AssnExpr]), apply_labels(Vars, Assns)
        ;
            adjust_level, smt2_op([assert, [not, AssnExpr]]), label(Vars)
        )
    ).

indomain(Var) :- label([Var]).


%
% initialization
%

executable(z3, path("z3"), ["-smt2", "-in"]).
executable(cvc4, path("cvc4"), ["-L", "smt2", "-i", "--smtlib-strict"]).

base_init_script([
    ['set-logic', 'QF_NIA'],
    ['set-option', ':produce-models', true]
]).

init_script(z3, [['set-option', ':smtlib2_compliant', true] | L]) :- !, base_init_script(L).
init_script(_, L) :- base_init_script(L).

terminate :-
    nb_getval(smt2_io, io(ToSMT2, _)) ->
        smt2_out(ToSMT2, [exit]), nl(ToSMT2);
    true.

init :-
    nb_setval(smt2_io, []), at_halt(terminate),
    solver(Solver),
    executable(Solver, Exe, Args),
    process_create(Exe, Args, [stdin(pipe(ToSMT2)), stdout(pipe(FromSMT2))]),
    nb_setval(smt2_io, io(ToSMT2, FromSMT2)),
    init_script(Solver, InitScript), maplist(smt2_op, InitScript),
    constraint_timeout_ms(ConstraintTimeoutMS), set_timeout(ConstraintTimeoutMS),
    nb_setval(smt2_io_level, 0),
    prolog_current_choice(Choice), nb_setval(smt2_pl_level, level(0, Choice)).

:- init.
